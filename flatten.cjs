module.exports = function flatten(elements, depth = 1) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    let flat = [];
    if(depth < 0) {
      depth = 0;
    }
    depth--;
    if(elements == undefined) {
        return flat;
    }
    if(!(elements instanceof Array)) {
      return flatten(Object.values(elements));
    }
    for (let index = 0; index < elements.length; index++) {
        if(elements[index] == undefined) continue;
        if(Array.isArray(elements[index]) && depth !== 0) {
          flat = flat.concat(flatten(elements[index], depth));
        } else {
          flat = flat.concat(elements[index]);
        }
    }
    return flat;
}

