const _ = require("underscore")

const map = (elements, cb) => {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
    let resArray = [];
    // Should only work with array
    if(!(elements instanceof Array) || elements == undefined || cb == undefined) {
        return resArray;
    }
    
    for (let index = 0; index < elements.length; index++) {
      const eleAtIndex = cb(elements[index], index, elements);
      resArray.push(eleAtIndex);
    }
    return resArray;
};

module.exports = map;