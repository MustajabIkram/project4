const _ = require("underscore")

const each = (elements, cb) => {
    // Do NOT use forEach to complete this function.
    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // This only needs to work with arrays.
    // You should also pass the index into `cb` as the second argument
    // based off http://underscorejs.org/#each
    let resArray = [];
    // Should only work with array
    if(!(elements instanceof Array) || elements == undefined || cb == undefined) {
        return resArray;
    }

    for (let i = 0; i < elements.length; i++) {
        resArray.push(cb(elements, i));
    }
    return resArray;

    // using underscore to iterate
    // return _.each(elements, cb);
};

module.exports = each;