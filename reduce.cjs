const _ = require("underscore")

const reduce = (elements, cb, startingValue) => {
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
    // Should only work with array
    if(elements instanceof Array) {
        let accumulator =  startingValue || elements[0];
        if(accumulator === elements[0]) {
            for( let index = 1; index < elements.length; index++) {
                accumulator = cb(accumulator, elements[index], index, elements);
            }
        } else {
            for( let index = 0; index < elements.length; index++) {
                accumulator = cb(accumulator, elements[index], index, elements);
            }
        } 
        return accumulator;
    } else {
        return;
    }
};

module.exports = reduce;