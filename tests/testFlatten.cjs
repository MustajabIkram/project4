const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'
const nestedArray1 = [[[[]]], ,[], [[]], [[[]]]]; // use this to test 'flatten'
const nestedArray2 = [[[[6]]], [6], [2, 5], [[5, 3]], [[[1, 4]]]]; // use this to test 'flatten'
const nestedArray3 = [[[[5]]],,[], [1, []], [1, [1, [1, [0]]]]]; // use this to test 'flatten'

const flat = require("../flatten.cjs")


const result = flat(nestedArray, 0);
console.log(result);
console.log(nestedArray.flat(0))

const result1 = flat(nestedArray1);
console.log(result1);
console.log(nestedArray1.flat())


const result2 = flat(nestedArray2, 4);
console.log(result2);
console.log(nestedArray2.flat(4))


const result3 = flat(nestedArray3);
console.log(result3);
console.log(nestedArray3.flat())


const result4 = flat([6, [5], [4], [], , [[[4]]], [3, [5, [5]], 2, 1, [6, 4, [76]]]]);
console.log(result4);

const result5 = flat([1, , [2], [[3]], [[[4]]]], -1);
console.log(result5);
console.log([1, , [2], [[3]], [[[4]]]].flat(-1));

const result6 = flat([1, , [2,, 3, [5,, 6, [3, [,,5, ]]]], [[3]], [[[4]]]], 1);
console.log(result6);
console.log([1, , [2,, 3, [5,, 6, [3, [,,5, ]]]], [[3]], [[[4]]]].flat(1));

const arrayLike = {
    length: 3,
    0: [1, 2],
    // Array-like objects aren't flattened
    1: { length: 2, 0: 3, 1: 4 },
    2: 5,
  };
  console.log(Array.prototype.flat.call(arrayLike));
  // [ 1, 2, { '0': 3, '1': 4, length: 2 }, 5 ]
  console.log(flat(arrayLike));