const map = require("../map.cjs")
let cb = () => [];
cb = ( x ) => {
    if(isNaN(x)) {
        console.log('Invalid Number')
    }
    if(x === undefined || x === null) return [];
    else return x;
};
cb = ( x, i ) => {
    if(isNaN(x) || isNaN(i)) {
        console.log('Invalid Number')
    }
    if(x === undefined || x === null) return [];
    if(i === undefined || i === null) return x;
    else return x;
};
cb = ( x, i, arr ) => {
    if(isNaN(x) || isNaN(i)) {
        console.log('Invalid Number')
    }
    if(x === undefined || x === null) return [];
    if(i === undefined || i === null) return x;
    if(arr === undefined || arr === null) return x;
    else return x;
};

const items = [1, 2, 3, 4, 5, 5];
const items1 = [1, "hi", true];
const items2 = ["1hi", "hola", "hello"];
const items3 = [true, false];
const items4 = [];
const items5 = {"key": "value"};
const items6 = [2.65, 5.25, true];

const result = map(items, cb);
console.log(result);

const result1 = map(items1, cb);
console.log(result1);

const result2 = map(items2, cb);
console.log(result2);

const result3 = map(items3, cb);
console.log(result3);

const result4 = map(items4, cb);
console.log(result4);

const result5 = map(items5, cb);
console.log(result5);

const result6 = map(items6, cb);
console.log(result6);

const result7 = map(cb);
console.log(result7);

const result8 = map(undefined, undefined);
console.log(result8);


const result9 = map(items, parseInt);
console.log(result9);

const result10 = map(items6, parseInt);
console.log(result10);
