const reduce = require("../reduce.cjs")
let cb = () => [];

const items = [1, 2, 3, 4, 5, 5];
const items1 = [1, "hi", true];
const items2 = ["hi", "hola", "hello"];
const items3 = [true, false];
const items4 = [];
const items5 = {"key": "value"};
const items6 = [2.65, 5.25, true];

const array1 = [1, 2, 3, 4];

cb = ( reduce, element, index, array ) => reduce += element;
const initialValue = 0;
const sumWithInitial = array1.reduce(
  (accumulator, currentValue) => accumulator + currentValue,
  initialValue
);
console.log(sumWithInitial)
const result = reduce(array1, (accumulator, currentValue) => accumulator + currentValue, initialValue);
console.log(result);

const result1 = reduce(items1, cb, 1);
console.log(result1);

const result2 = reduce(items2, cb, {});
console.log(result2);

const result3 = reduce(items3, cb, true);
console.log(result3);

const result4 = reduce(items4, cb);
console.log(result4);

const result5 = reduce(items5, cb);
console.log(result5);

const result6 = reduce(items6, cb, 2.65);
console.log(result6);

const result7 = reduce(cb);
console.log(result7);

const result8 = reduce();
console.log(result8);

const getMax = (a, b) => Math.max(a, b);

// callback is invoked for each element in the array starting at index 0
console.log([1, 100].reduce(getMax, 50), reduce([1, 100], getMax, 50)); // 100
console.log([50].reduce(getMax, 10), reduce([50], getMax, 10)); // 50

// callback is invoked once for element at index 1
console.log([1, 100].reduce(getMax), reduce([1, 100], getMax)); // 100

// callback is not invoked
console.log([50].reduce(getMax), reduce([50], getMax)); // 50
console.log([].reduce(getMax, 1), reduce([], getMax, 1)); // 1

// [].reduce(getMax); // TypeError
console.log(reduce([], getMax));

const array = [15, 16, 17, 18, 19];

function reducer(accumulator, currentValue, index) {
  const returns = accumulator + currentValue;
  console.log(
    `accumulator: ${accumulator}, currentValue: ${currentValue}, index: ${index}, returns: ${returns}`,
  );
  return returns;
}

console.log(array.reduce(reducer), reduce(array, reducer));

const objects = [{ x: 1 }, { x: 2 }, { x: 3 }];
const sum = objects.reduce(
  (accumulator, currentValue) => accumulator + currentValue.x,
  0,
);

console.log(sum, reduce(objects,  (accumulator, currentValue) => accumulator + currentValue.x, 0)); // 6
// const flattened = [
//     [0, 1],
//     [2, 3],
//     [4, 5],
//   ].reduce((accumulator, currentValue) => accumulator.concat(currentValue), []);
//   // flattened is [0, 1, 2, 3, 4, 5]
// console.log(flattened, reduce(flattened, ((accumulator, currentValue) => accumulator.concat(currentValue), [])))  

const names = ["Alice", "Bob", "Tiff", "Bruce", "Alice"];

const countedNames = names.reduce((allNames, name) => {
  const currCount = allNames[name] ?? 0;
  return {
    ...allNames,
    [name]: currCount + 1,
  };
}, {});
// countedNames is:
// { 'Alice': 2, 'Bob': 1, 'Tiff': 1, 'Bruce': 1 }
console.log(countedNames, reduce(names, (allNames, name) => {
    const currCount = allNames[name] ?? 0;
        return {
        ...allNames,
        [name]: currCount + 1,
        };
    }, {}));