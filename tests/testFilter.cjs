const filter = require("../filter.cjs")
let cb = () => "";

const items = [1, -2, 3, -4, 5, 5];
const items1 = [1, "hi", true];
const items2 = ["hi", "hola", "hello"];
const items3 = [true, false];
const items4 = [];
const items5 = {"key": "value"};
const items6 = [2.65, 5.25, true];

cb = ( x ) => x > 0;
cb = ( x ) => {
    if(isNaN(x)) {
        console.log('Invalid Number')
    }
    if(x === undefined || x === null) return NaN;
    return x > 0;
};
cb = ( x, i ) => {
    if(isNaN(x) || isNaN(i)) {
        console.log('Invalid Number')
    }
    if(x === undefined || x === null) return NaN;
    if(i === undefined || i === null) return NaN;
    return x > 0;
};
cb = ( x, i, arr ) => {
    if(isNaN(x) || isNaN(i)) {
        console.log('Invalid Number')
    }
    if(x === undefined || x === null) return NaN;
    if(i === undefined || i === null) return NaN;
    if(arr === undefined || arr === null) return NaN;
    return x > 0;
};

const result = filter(items, cb);
console.log(result);

const result1 = filter(items1, cb);
console.log(result1);


const result2 = filter(items2, cb);
console.log(result2);


const result3 = filter(items3, cb);
console.log(result3);


const result4 = filter(items4, cb);
console.log(result4);


const result5 = filter(items5, cb);
console.log(result5);


const result6 = filter(items6, cb);
console.log(result6);


const result7 = filter(cb);
console.log(result7);


const result8 = filter();
console.log(result8);

words = ["spray", "limit", "exuberant", "destruction", "elite", "present"];
const appendedWords = words.filter((word, index, arr) => {
  arr.push("new");
  return word.length < 6;
});
cb = (word, index, arr) => {
    arr.push("new")
    return word.length < 6;
  };
const result9 = filter(words, cb);
console.log(result9)