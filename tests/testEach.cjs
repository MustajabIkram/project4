const each = require("../each.cjs")
let cb = () => "";
cb = ( x ) => x;
cb = ( x, i) => x[i];

const items = [1, 2, 3, 4, 5, 5];
const items1 = [1, "hi", true];
const items2 = ["hi", "hola", "hello"];
const items3 = [true, false];
const items4 = [];
const items5 = {"key": "value"};
const items6 = [2.65, 5.25, true];

const result = each(items, cb);
console.log(result);

const result1 = each(items1, cb);
console.log(result1);

const result2 = each(items2, cb);
console.log(result2);

const result3 = each(items3, cb);
console.log(result3);

const result4 = each(items4, cb);
console.log(result4);

const result5 = each(items5, cb);
console.log(result5);

const result6 = each(items6, cb);
console.log(result6);

const result7 = each(cb);
console.log(result7);

const result8 = each();
console.log(result8);