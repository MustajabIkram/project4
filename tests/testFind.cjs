const find = require("../find.cjs")
let cb = () => "";

const items = [1, 2, 3, 4, 5, 5];
const items1 = [1, "hi", true];
const items2 = ["hi", "hola", "hello"];
const items3 = [true, false];
const items4 = [];
const items5 = {"key": "value"};
const items6 = [2.65, 5.25, true];

cb = ( x, valueToCompare ) => x === valueToCompare ? true : false;
const result = find(items, cb, 5);
console.log(result);

const result1 = find(items1, cb, "ho");
console.log(result1);

const result2 = find(items2, cb, "hi");
console.log(result2);

const result3 = find(items3, cb, true);
console.log(result3);

const result4 = find(items4, cb);
console.log(result4);

const result5 = find(items5, cb, 'hi');
console.log(result5);

const result6 = find(items6, cb);
console.log(result6);

const result7 = find(cb);
console.log(result7);

const result8 = find();
console.log(result8);