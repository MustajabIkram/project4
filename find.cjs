module.exports = function find(elements, cb, eleTofind) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
    let found = undefined;

    if(!(elements instanceof Array) || elements == undefined || cb == undefined || eleTofind == undefined || elements.length == 0) {
        return found;
    }

    for ( let index = 0; index < elements.length; ++index) {
        if ( cb(elements[index], eleTofind) ) { return found = elements[index] }
    }
    return found;
} 