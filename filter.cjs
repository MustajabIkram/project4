module.exports = function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
    let filteredArr = [];

    if(!(elements instanceof Array) || elements == undefined || cb == undefined || elements.length == 0) {
        return filteredArr;
    }
    let original = elements.length;
    for ( let index = 0; index < original; index++ ){
        cb(elements[index], index, elements) === true ? filteredArr.push(elements[index]) : null;
    }
    return filteredArr;
} 